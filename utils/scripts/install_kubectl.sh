#!/bin/bash

#vars 
COMP_NAME=kubectl
# FILE_EXT='linux-amd64.tar.gz'
APP_DIR='/usr/local/bin'
TEMP_DIR='/tmp'

curl -Lo ${TEMP_DIR}/${COMP_NAME} "https://dl.k8s.io/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl" && \
sudo mv ${TEMP_DIR}/${COMP_NAME} ${APP_DIR}/${COMP_NAME} && \
chmod +x  ${APP_DIR}/${COMP_NAME}

which ${COMP_NAME}