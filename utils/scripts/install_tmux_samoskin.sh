#!/bin/bash
#vars 
COMP_NAME=tmux
cd
# from my repo 
git clone https://github.com/frankperrakis/tmux-config.git
./tmux-config/install.sh
which ${COMP_NAME}
